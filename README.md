# Scroll liberator
Liberate scrollwheel from abusive sites.

## Examples
* https://alvarotrigo.com/fullPage/
* https://www.colgate.com/en-us/oral-health
* https://www.tesla.com/model3
* https://www.digitalrepublik.com/digital-marketing-newsletter/
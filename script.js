//var regex = /https?:\/\/(?:(?:www)|(?:encrypted))\.google(?:\.\w*){1,2}\/(?:(?:search)|(?:\?gws_rd=ssl)|(?:#q=)|(?:.*&q=)).*/;

// monkey patch builtin listener
var eventBlacklist = new Set(['scroll', 'wheel', 'mousewheel', 'DOMMouseScroll']);
function patchAEL(elm, elmName) {
	var proxy = new Proxy(elm.addEventListener, {
		apply: function(target, thisArg, argumentsList) {
			if (eventBlacklist.has(argumentsList[0])) {
				console.log('scrolling event blacklisted on ' + elmName);
				// if blacklisted, we may need to restore the bar by resetting overflow
				restoreOverflow();
				return;
			}
			target.apply(thisArg, argumentsList);
		}
	});
	exportFunction(proxy, elm, {defineAs:'addEventListener'});
}
patchAEL(document, 'document');
patchAEL(window, 'window');
patchAEL(document.body, 'body');

var restoreOverflowStarted = false;
function restoreOverflow() {
	if (restoreOverflowStarted == true) {
		return;
	}
	restoreOverflowStarted = true;
	var attempts = 0;
	function process() {
		attempts ++;
		try {
			document.body.style.overflow = 'auto';
		} catch (e) {}
		if (attempts > 200) {
			return;
		}
		setTimeout(process, 50 + attempts);
	}
	process();
}

// monkey patch jquery
var jqueryBlacklist = new Set(['mousewheel', 'scroll']);
process();
function process() {
	attempts ++;
	if (!!window.wrappedJSObject.$) {
		patchJQuery();
	} else {
	  if (attempts > 200) {
		return;
	  }
	  setTimeout(process, 50 + attempts);
	}
}

function nop() {
	console.log('scrolling event blacklisted: jquery prop');
}

function patchJQuery() {
	// the main jquery func
	var proxy$ = new Proxy(window.wrappedJSObject.$, {
		apply: function(target, thisArg, argumentsList) {
			var result = target.apply(thisArg, argumentsList);
			jqueryBlacklist.forEach(blacklistedEvent => {
				exportFunction(nop, result, {defineAs: blacklistedEvent});
			});
			proxyOn = new Proxy(result.on, {
				apply: function(target, thisArg, argumentsList) {
					if (jqueryBlacklist.has(argumentsList[0])) {
						console.log('scrolling event blacklisted: jquery on');
						return;
					}
					return target.apply(thisArg, argumentsList);
				}
			});
			exportFunction(proxyOn, result, {defineAs: 'on'});
			return result;
		}
	});
	exportFunction(proxy$, window, {defineAs:'$'});
}